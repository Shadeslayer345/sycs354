/** Copyright 2015 Boluwatife Aiki-Raji Errol Grannum Barry C. Harris Jr. */
#include "ADT/BST.h"
#include "ADT/SNP.h"
#include <iostream>
#include <fstream>
#include <string>

int main() {
  std::string tree_name;
  std::string genome_path;
  std::string headerLine;
  BST DnaTree;
  SNP snip;
  std::ifstream genomeFile;

  std::cout << "Welcome to the DNA Trees Data Analyzer" << std::endl;
  std::cout << "To begin, please name your custom tree" << std::endl;
  getline(std::cin, tree_name);
  std::cout << "Thank you, Please enter the path of the file that contains your genes"<< std::endl;
  getline(std::cin, genome_path);

  // user enters a name for their DNA Tree
  DnaTree.set_name(tree_name);


  // open the file from the path entered by the user
  genomeFile.open(genome_path.c_str());

  // getline, to ignore the first line
  getline(genomeFile, headerLine);

  for(int i = 0; i < 114; i++) {
    int new_rsid, new_chromosome, new_location;
    std::string rsid_temp;
    std::string new_genotype;

    genomeFile >> rsid_temp >> new_chromosome >> new_location >> new_genotype;

    rsid_temp = rsid_temp.substr(2);
    new_rsid = std::stoi(rsid_temp);

    snip.set_rsid(new_rsid);
    snip.set_chromosome(new_chromosome); 
    snip.set_location(new_location); 
    snip.set_genotype(new_genotype);

    // add a nucleotide to the tree
    DnaTree.add_snip(snip);
  }

  std::cout << std::endl << "Finished analyzing your DNA ;)" << std::endl << std::endl <<
          "Beginnig Tree construction" << std::endl << std::endl;

  // print the tree
  std::cout << "PRESENTING YOUR DNA!" << std::endl << std::endl;
  std::cout << tree_name << std::endl << std::endl;
  //DnaTree.print();

  // retrieve a SNP by its' RSID
  //DnaTree.find('r', 4477212, " ");

  // retrieve a SNP by it's location
  //DnaTree.find('l', 904165, " ");

  // retrieve a SNP by it's genotype
  // DnaTree.find('g', 0, "AA");

  // delete the tree
  // DnaTree.destroy();
  return 0;
}
