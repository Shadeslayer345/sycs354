/** Copyright 2015 Boluwatife Aiki-Raji Errol Grannum Barry C. Harris Jr. */
#include "Node.h"
#include "SNP.h"
#include <iostream>

#ifndef PROJECTS_DNATREES_ADT_BST_H
#define PROJECTS_DNATREES_ADT_BST_H
class BST {
  public:
    BST();
    ~BST();

    int compare(const SNP& snip_one, const SNP& snip_two);
    void set_name(std::string name);
    
    void add_snip(SNP& next_snip);
    void find(char type, int key_num, std::string key_string);

    void destroy();
    void print();

    std::string name() const;
 private:
    Node* root;
    std::string name_;

    void add_snip(SNP& next_snip, Node *leaf);
    void find_by_rsid(int key, Node *leaf);
    void find_by_location(int key, Node *leaf);
    void find_by_genotype(std::string, Node *leaf);
    void destroy(Node *leaf);
    void print(Node *leaf);
};
#endif
