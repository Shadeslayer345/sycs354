/** Copyright 2015 Boluwatife Aiki-Raji Errol Grannum Barry C. Harris Jr. */
#include "Node.h"

Node::Node() {
    right_ = NULL;
    left_ = NULL;
    this->data_.set_rsid(0);
    this->data_.set_location(0);
    this->data_.set_chromosome(0);
    this->data_.set_genotype(" ");
    this->data_.set_phenotype(" ");
}

/**
 * Returns the snip object.
 */
SNP Node::data() {
    return data_;
}

/**
 * Displays all snip data members
 */
void Node::print_data() {
    data_.print();
}


/**
 * Retrieves the left node from the current node on the tree.
 * @return The successor node
 */
Node* Node::left() {
    return left_;
}

/**
 * Retrieves the right node from the current node on the tree.
 * @return The successor node
 */
Node* Node::right() {
    return right_;
}

/**
 * Sets data for new node.
 * @param data SNP object that will serve as the data in the node
 */
void Node::set_data(SNP& data) {
    this->data_ = data;
}

/**
 * Sets the left element in memory for the current node on the tree.
 * @param next The left element
 */
void Node::set_left(Node *left) {
    left_ = left;
}

/**
 * Sets the right element in memory for the current node on the tree.
 * @param right The right element
 */
void Node::set_right(Node *right) {
    right_ = right;
}
