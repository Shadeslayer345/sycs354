/** Copyright 2015 Barry C. Harris Jr. Boluwatife Aiki-Raji Errol Grannum */
#include <iostream>
#include "SNP.h"

#ifndef PROJECTS_DNATREES_ADT_NODE_H
#define PROJECTS_DNATREES_ADT_NODE_H
class Node {
 public:
    Node();
    SNP data();
    void print_data();

    Node* left();
    Node* right();

    void set_data(SNP& data);
    void set_left(Node *left);
    void set_right(Node *right);
 private:
    SNP data_;
    Node* left_;
    Node* right_;
};
#endif
