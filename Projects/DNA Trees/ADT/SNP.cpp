/** Copyright 2015 Boluwatife Aiki-Raji Errol Grannum Barry C. Harris Jr. */
#include "SNP.h"

/**
 * Default constructor for a snip that explicitly sets all data members.
 * @constructor
 */
SNP::SNP() {
    rsid_ = 0;
    location_ = 0;
    chromosome_ = 0;
    genotype_ = " ";
    phenotype_ = " ";
}

/**
 * Multiparameter constructor that enables the user to pass in information for
 *     specific data members.
 * @param new_rsid New rsid value
 * @param new_location New location value
 * @param new_chromosome New chromosome value
 * @param new_genotype New genotype value
 * @param new_phenotype New phenotype value
 * @constructor
 */
SNP::SNP(int new_rsid, int new_location, int new_chromosome,
        std::string new_genotype, std::string new_phenotype) {
    rsid_ = new_rsid;
    location_ = new_location;
    chromosome_ = new_chromosome;
    genotype_ = new_genotype;
    phenotype_ = new_phenotype;
}

/**
 * Changes the value of the rsid data member.
 * @param new_rsid The new rsid value
 */
void SNP::set_rsid(int new_rsid) {
    rsid_ = new_rsid;
}

/**
 * Changes the value of the location data member.
 * @param new_location The new location value
 */
void SNP::set_location(int new_location) {
    location_ = new_location;
}

/**
 * Changes the value of the chromosome data member.
 * @param chromosome The new chromosome value
 */
void SNP::set_chromosome(int new_chromosome) {
    chromosome_ = new_chromosome;
}

/**
 * Changes the value of the genotype data member.
 * @param new_genotype The new genotype value
 */
void SNP::set_genotype(std::string new_genotype) {
    genotype_ = new_genotype;
}

/**
 * Changes the value of the phenotype data member.
 * @param new_phenotype The new phenotype value
 */
void SNP::set_phenotype(std::string new_phenotype) {
    phenotype_ = new_phenotype;
}

/**
 * Returns the rsid data member.
 * @return The rsid value
 */
int SNP::rsid() const {
    return rsid_;
}

/**
 * Returns the location data member.
 * @return The location value
 */
int SNP::location() const {
    return location_;
}

/**
 * Returns the chromosome data member.
 * @return The chromosome value
 */
int SNP::chromosome() const {
    return chromosome_;
}

/**
 * Returns the genotype data member.
 * @return The genotype value
 */
std::string SNP::genotype() const {
    return genotype_;
}

/**
 * Returns the phenotype data member.
 * @return The phenotype value
 */
std::string SNP::phenotype() const {
    return phenotype_;
}

/**
 * Displays all data members of a snip.
 */
void SNP::print() {
    std::cout << "SNIP" << std::endl <<
            "RSID: " << rsid_ << std::endl <<
            "Chromosome: " << chromosome_ << std::endl <<
            "Location: " << location_ << std::endl <<
            "Genotype: " << genotype_ << std::endl;
            //"Phenotype: " << phenotype_ << std::endl;
}
