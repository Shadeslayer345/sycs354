/** Copyright 2015 Boluwatife Aiki-Raji Errol Grannum Barry C. Harris Jr. */
#include "BST.h"

BST::BST() {
    root = NULL;
}

BST::~BST() {
    destroy();
}
/**
 * Compare method to be used when placing new snips into the BST. Uses the
 *     rsid as a comparator.
 * @param  snip_one SNP object
 * @param  snip_two SNP object
 * @return 0 for snip_one == snip_two
 *         1 for snip_one > snip_two
 *         -1 for snip_one < snip_two
 */
int BST::compare(const SNP& snip_one, const SNP& snip_two) {
    if (snip_one.rsid() == snip_two.rsid()) {
        return 0;
    } else if (snip_one.rsid() > snip_two.rsid()) {
        return 1;
    } else {
        return -1;
    }
}

/**
 * Set's the test subject's name for the BST.
 * @param name test subject's name
 */
void BST::set_name(std::string name) {
    name_ = name;
}

/**
 * Adds a new snip to the BST.
 * @param next_snip SNP object
 */
void BST::add_snip(SNP& next_snip) {
    if (root != NULL) {
        add_snip(next_snip, root);
    } else {
        root = new Node;
        root->set_data(next_snip);
        root->set_left(NULL);
        root->set_right(NULL);
    }
}

/**
 * Adds a new snip to the BST.
 * @param next_snip SNP object
 * @param leaf Root node in BST
 * @private
 */
void BST::add_snip(SNP& next_snip, Node *leaf) {
    if (compare(next_snip, leaf->data()) == -1) {
        if (leaf->left() != NULL) {
            add_snip(next_snip, leaf->left());
        } else {
            leaf->set_left(new Node);
            leaf->left()->set_data(next_snip);
            leaf->left()->set_left(NULL);
            leaf->left()->set_right(NULL);
        }
    } else if (compare(next_snip, leaf->data()) == 1) {
        if (leaf->right() != NULL) {
            add_snip(next_snip, leaf->right());
        } else {
            leaf->set_right(new Node);
            leaf->right()->set_data(next_snip);
            leaf->right()->set_left(NULL);
            leaf->right()->set_right(NULL);
        }
    }
}

/**
 * Parent function for calling other find functions.
 * @param type       [description]
 * @param key_num    Optional: the rsid or location to search for
 * @param key_string Optional: the phenotype or genotype to search for
 */
void BST::find(char type, int key_num = 0, std::string key_string = " ") {
    if (type == 'r') {
        find_by_rsid(key_num, root);
    } else if (type == 'l') {
        find_by_location(key_num, root);
    } else {
        find_by_genotype(key_string, root);
    }
}

/**
 * Indirect recursive function for finding a snip by rsid
 * @param key search criteria, rsid
 */
void BST::find_by_rsid(int key, Node *leaf) {
    if(leaf != NULL) {
        if(key == leaf->data().rsid()) {
            std::cout << std::endl << "FOUND SNIP " << std::endl;
            leaf->data().print();
            std::cout << std::endl;
        }
        if(key < leaf->data().rsid())
            return find_by_rsid(key, leaf->left());
        else
            return find_by_rsid(key, leaf->right());
      } //else if (leaf == NULL)
         // std::cout << "Not found." << std::endl;
}

/**
 * Indirect recursive function for finding a snip by location
 * @param key search criteria, location
 */
void BST::find_by_location(int key, Node *leaf) {
    if (leaf != NULL) { 
        if(key == leaf->data().location()) {
            std::cout << std::endl << "FOUND SNIP " << std::endl;
            leaf->data().print();
            std::cout << std::endl;
        } if(key < leaf->data().location())
            return find_by_location(key, leaf->left());
        else
            return find_by_location(key, leaf->right());
    } //else if (leaf == NULL) 
       // std::cout << "Not found." << std::endl;
}

void BST::find_by_genotype(std::string key, Node *leaf) {
    if (leaf != NULL) {
        if(key.compare(leaf->data().genotype()) == 0) {
            std::cout << std::endl << "FOUND SNIP " << std::endl;
            leaf->data().print();
            std::cout << std::endl;
        } if(key.compare(leaf->data().genotype()) < 0)
            return find_by_genotype(key, leaf->left());
        else
            return find_by_genotype(key, leaf->right());
    } //else if (leaf == NULL)
        // std::cout << "Not found." << std::endl;
}

/**
 * Indirect recursive call to output the BST.
 */
void BST::print() {
    print(root);
}

/**
 * Traverses the BST in (post) order and prints information from each snip.
 * @private
 */
void BST::print(Node *leaf) {
    if(leaf != NULL) {
        if(leaf->left()) print(leaf->left());
        if(leaf->right()) print(leaf->right());
        std::cout << "      ";
        leaf->data().print();
        std::cout << std::endl;
    }
}

/**
 * Destroys all nodes in the BST.
 */
void BST::destroy() {
    destroy(root);
}

/**
 * Destroys BST.
 */
void BST::destroy(Node *leaf) {
    if (leaf != NULL)
    {
        destroy(leaf->left());
        destroy(leaf->right());
        delete leaf;
    }
}

/**
 * Returns the name of the BST.
 * @return name of BST
 */
std::string BST::name() const {
    return name_;
}
