/** Copyright 2015 Boluwatife Aiki-Raji Errol Grannum Barry C. Harris Jr. */
#include <iostream>

#ifndef PROJECTS_DNATREES_ADT_SNP_H
#define PROJECTS_DNATREES_ADT_SNP_H
class SNP {
 public:
    SNP();
    SNP(int new_rsid, int new_location, int new_chromosome,
        std::string new_genotype, std::string new_phenotype);

    void set_rsid(int new_rsid);
    void set_location(int new_location);
    void set_chromosome(int new_chromosome);
    void set_genotype(std::string new_genotype);
    void set_phenotype(std::string new_phenotype);

    int rsid() const;
    int location() const;
    int chromosome() const;
    std::string genotype() const;
    std::string phenotype() const;

    void print();
 private:
    int rsid_, location_, chromosome_;
    std::string genotype_, phenotype_;
};
#endif
