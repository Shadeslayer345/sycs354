#ifndef GROUP7_BUBBLESORT_H
#define GROUP7_BUBBLESORT_H
class BubbleSort {
 public:
    void sort(int input[], int length);
};
#endif

#ifndef GROUP7_STOOGESORT_H
#define GROUP7_STOOGESORT_H
class StoogeSort {
 public:
    void sort(int input[], int first, int last);
};
#endif

/**
 * Performing a Bubble sort on an array of specified length. Starting with
 *         the first element, we will compare that element with every element
 *         in the array until we find the element that is greater than our
 *         reference element, swapping elements as we move along such that after
 *         our final pass the array will have the largest element at the back of
 *         the array and the lighest element will have "Bubbled"  to the front.
 * @param input  The array to sort
 * @param length The number of elements in the array
 */
void BubbleSort::sort(int input[], int length) {
    for (int i = 0; i < length; i++) {
        for (int j = i + 1; j < length; j++) {
            if (input[i] > input[j]) {
                int temp = input[i];
                input[i] = input[j];
                input[j] = temp;
            }
        }
    }
}

/**
 * Performing a Stooge sort on an array. We employ a recursive algorithm that
 *         checks for one situation: If the value at the front of the array is
 *         larger than the value at the back. If true than a swap is employed.
 *         The recursive part of the algorithm checks how many elements are in
 *         the array, if there are more than three we follow these steps:
 *                 i) Sort the first 2/3 of the array
 *                 ii) Sort the last 2/3 of the array
 *                 iii) Sor the first 2/3 of the array
 * @param input The array to be sorted
 * @param first The first index of the sort algorithm
 * @param last  The last index to be sorted
 */
void StoogeSort::sort(int input[], int first, int last) {
    int t;
    int i = first;
    int j = last;
    if (input[j] < input[i]) {
        int temp = input[i];
        input[i] = input[j];
        input[j] = temp;
    }
    if ((j - i + 1) > 2) {
        t = (j - i + 1) / 3;
        sort(input, i, j-t);
        sort(input, i + t, j);
        sort(input, i, j - t);
    }
}
