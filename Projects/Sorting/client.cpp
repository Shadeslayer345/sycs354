#include <iostream>

#include "Group7.h"

int main() {
    std::cout << "Unsorted Array:" << std::endl;
    int arr[5] = {10, 24, 13, 1, 5};
    int arr2[5] = {10, 24, 13, 1, 5};

    for (int i = 0; i < 5; ++i) {
        std::cout << arr[i] << " ";
    }

    std::cout << std::endl << std::endl << "BubbleSort:" << std::endl;
    BubbleSort bsort;
    bsort.sort(arr, 5);
    for (int i = 0; i < 5; ++i) {
        std::cout << arr[i] << " ";
    }

    std::cout << std::endl << std::endl << "Stooge Sort:" << std::endl;
    StoogeSort ssort;
    ssort.sort(arr2, 0, 4);
    for (int i =0; i < 5; ++i) {
        std::cout << arr2[i] << " ";
    }
    std::cout << std::endl;

    return 0;
}
