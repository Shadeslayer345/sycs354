#Group 7


##BUBBLE SORT
####The BubbleSort class has a single method, "sort", that sorts an array. The function takes in an array and the array's length as parameters.


##STOOGE SORT
####The StoogeSort class has a single method, "sort", that sorts an array. The function takes in an array, the first index and also the last index to sort.
###To use:

```
    #include "Group7.h"

    ...

    BubbleSort bubble;
    StoogeSort stooge;

    bubble.sort(input_array_one, length);
    stooge.sort(input_array_two, first, (length of input_array_two) - 1);
```

For convience a sample file (client.cpp) is included.
