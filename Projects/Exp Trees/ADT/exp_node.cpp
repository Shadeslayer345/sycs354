/** Copyright 2015 Boluwatife Aiki-Raji Errol Grannum Barry C. Harris Jr. */
#include "exp_node.h"

/**
 * Operand node constructor for Expression Tree.
 * @constructor
 */
exp_node::exp_node(int value) {
   kind = NUMBER;
   number = value;
};

/**
 * Operator node constructor for Expression Tree.
 * @constructor
 */
exp_node::exp_node( char op, exp_node *left, exp_node *right ) {
   kind = OPERATOR;
   this->op = op;
   this->left = left;
   this->right = right; 
}

/**
 * Returns the value of the node.
 * @return 
 *     the integer value of an operand
 *     the evaluation of the expression is an operator
 */
int exp_node::getValue() {
  if (kind == NUMBER) {
    return number;
  } else {
      int leftVal = left->getValue();
      int rightVal = right->getValue();
      switch (op) {
        case '+': return leftVal + rightVal;
        case '-': return leftVal - rightVal;
        case '*': return leftVal * rightVal;
        case '/': return leftVal / rightVal;
        case '^': return pow(leftVal, rightVal);
        default: return 0;
       }
    }
 }
