#include <queue>
#include <cstddef>
#include <iostream>
#include <iomanip>
#include <deque>

#include "exp_node.h"

#ifndef PROJECTS_EXPTREES_ADT_BTREE_H
#define PROJECTS_EXPTREES_ADT_BTREE_H
/**
 * Template for Binary Tree operations and data.
 */
class btree {
 public:
    btree();
    ~btree();
    void insert(exp_node* last_op);
    void printBranches(int branchLen, int nodeSpaceLen, int startLen, int nodesInThisLevel, const std::deque<exp_node*>& nodesQueue);
    void printNodes(int branchLen, int nodeSpaceLen, int startLen, int nodesInThisLevel, const std::deque<exp_node*>& nodesQueue);
    void printLeaves(int indentSpace, int level, int nodesInThisLevel, const std::deque<exp_node*>& nodesQueue);
    void printPretty(int level, int indentSpace);
    int height(exp_node* node);
 private:
    exp_node* root;
};
#endif
