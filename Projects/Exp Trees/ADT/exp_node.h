#include <cmath>

#ifndef PROJECTS_EXPTREES_ADT_EXPNODE_H
#define PROJECTS_EXPTREES_ADT_EXPNODE_H
const int NUMBER = 0,    // Values representing two kinds of nodes.
          OPERATOR = 1;

/**
 * 
 * @constructor
 */
struct exp_node {  // A node in an expression tree.
    exp_node(int value);
    exp_node(char op, exp_node* left, exp_node* right);    

    int getValue();
    int kind;
    int number;
    char op;
    exp_node *left;
    exp_node *right;
 };
#endif
