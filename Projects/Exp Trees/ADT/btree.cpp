/** Copyright 2015 Boluwatife Aiki-Raji Errol Grannum Barry C. Harris Jr. */  
#include "btree.h"

btree::btree() {
  this->root = NULL;
}

btree::~btree() {
  delete root;
}

void btree::insert(exp_node* last_op) {
  root = last_op;
}

// Print the arm branches (eg, /    \ ) on a line
void btree::printBranches(int branchLen, int nodeSpaceLen, int startLen, int nodesInThisLevel, const std::deque<exp_node*>& nodesQueue) {
  std::deque<exp_node*>::const_iterator iter = nodesQueue.begin();
  for (int i = 0; i < nodesInThisLevel / 2; i++) {
    if ((*iter)) {
      std::cout << ((i == 0) ? std::setw(startLen-1) : std::setw(nodeSpaceLen-2)) << "" << ((*iter++) ? "/" : " ");
      std::cout << std::setw(2*branchLen+2) << "" << ((*iter++) ? "\\" : " ");
    }
  }
  std::cout << std::endl;
}

// Print the branches and node (eg, ___10___ )
void btree::printNodes(int branchLen, int nodeSpaceLen, int startLen, int nodesInThisLevel, const std::deque<exp_node*>& nodesQueue) {
  std::deque<exp_node*>::const_iterator iter = nodesQueue.begin();
  for (int i = 0; i < nodesInThisLevel; i++, iter++) {
    if ((*iter) && (*iter)->kind == NUMBER) {
        std::cout << ((i == 0) ? std::setw(startLen) : std::setw(nodeSpaceLen)) << "" << std::setfill(' ');
        std::cout << std::setw(branchLen+2) << (*iter)->number;
        std::cout << std::setfill(' ') << std::setw(branchLen) << "" << std::setfill(' ');
    } else if ((*iter) && (*iter)->kind == OPERATOR){
      std::cout << ((i == 0) ? std::setw(startLen) : std::setw(nodeSpaceLen)) << "" << ((*iter && (*iter)->left) ? std::setfill('_') : std::setfill(' '));
      std::cout << std::setw(branchLen+2) << ((*iter) ? (*iter)->op : '\0');
      std::cout << ((*iter && (*iter)->right) ? std::setfill('_') : std::setfill(' ')) << std::setw(branchLen) << "" << std::setfill(' ');
    }
  }
  std::cout << std::endl;
}

// Print the leaves only (just for the bottom row)
void btree::printLeaves(int indentSpace, int level, int nodesInThisLevel, const std::deque<exp_node*>& nodesQueue) {
  std::deque<exp_node*>::const_iterator iter = nodesQueue.begin();
  for (int i = 0; i < nodesInThisLevel; i++, iter++) {
    if ((*iter) && (*iter)->number)
      std::cout << ((i == 0) ? std::setw(indentSpace+2) : std::setw(2*level+2)) << (*iter)->number;
  }
}

/* Compute the "height" of a tree -- the number of
    nodes along the longest path from the root node
    down to the farthest leaf node.*/
int btree::height(exp_node* node) {
  if (node == NULL) {
    return 0;
  } else {
    /* compute the height of each subtree */
    int lheight = (node->kind == OPERATOR) ? height(node->left) : 0;
    int rheight = (node->kind == OPERATOR) ? height(node->right) : 0;
    /* use the larger one */
    return (lheight > rheight) ? (lheight + 1) : (rheight + 1);
  }
}

void btree::printPretty(int level, int indentSpace) {
  int h = height(root);
  int nodesInThisLevel = 1;

  int branchLen = 2*((int)pow(2.0,h)-1) - (3-level)*(int)pow(2.0,h-1);  // eq of the length of branch for each node of each level
  int nodeSpaceLen = 2 + (level+1)*(int)pow(2.0,h);  // distance between left neighbor node's right arm and right neighbor node's left arm
  int startLen = branchLen + (3-level) + indentSpace;  // starting space to the first node to print of each level (for the left most node of each level only)
    
  std::deque<exp_node*> nodesQueue;
  nodesQueue.push_back(root);
  for (int r = 1; r < h; r++) {
    printBranches(branchLen, nodeSpaceLen, startLen, nodesInThisLevel, nodesQueue);
    branchLen = branchLen/2 - 1;
    nodeSpaceLen = nodeSpaceLen/2 + 1;
    startLen = branchLen + (3-level) + indentSpace;
    printNodes(branchLen, nodeSpaceLen, startLen, nodesInThisLevel, nodesQueue);

    for (int i = 0; i < nodesInThisLevel; i++) {
      exp_node *currNode = nodesQueue.front();
      nodesQueue.pop_front();
      if (currNode) {
        nodesQueue.push_back(currNode->left);
        nodesQueue.push_back(currNode->right);
      } else {
        nodesQueue.push_back(NULL);
        nodesQueue.push_back(NULL);
      }
    }
    nodesInThisLevel *= 2;
  }
  printBranches(branchLen, nodeSpaceLen, startLen, nodesInThisLevel, nodesQueue);
  printLeaves(indentSpace, level, nodesInThisLevel, nodesQueue);
}
