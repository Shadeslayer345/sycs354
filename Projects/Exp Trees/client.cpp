/** Copyright 2015 Boluwatife Aiki-Raji Errol Grannum Barry C. Harris Jr. */
#include <stack>
#include <string>

#include "ADT/btree.h"

void get_input();
std::string in_to_post(std::string infix);
bool is_num(char element);
bool is_op(char element);
int get_precedence(char element);
int evaluate(std::string postfix);
int evaluate(char op, int x, int y);
void print_tree(std::string postfix);

int main() {
    std::cout << "This program works only with simple expressions" << std::endl;
    std::cout <<"(expressions with only one pair of parenthesis and a " <<
            "limited amount of operations)" << std::endl;
    std::cout << "A tested example of this is (1+2)*3" << std::endl;
    std::cout << "----------------------------------------------------------" <<
            "-----------" << std::endl << std::endl << std::endl;
    get_input();
    return 0;
}

void get_input() {
    std::string expression;

    std::cout << "Welcome to the Expression trees!" << std::endl <<
        "--------------------------------------" << std::endl << 
        "Please enter an equation: ";

    std::getline(std::cin, expression);
    /**
     * Need algorithm to separate all operators and variables in expression
     *     and insert into the tree
     */
    
    std::cout << "What would you like to do with the expression? " << std::endl
        << "(1) Convert to RPN    (2) Evaluate    (3) Print the syntax tree: ";

    int choice;
    std::cin >> choice;
    std::cin.get();

    std::string postfix = in_to_post(expression);

    switch(choice) {
        case 1: std::cout << "Converting to RPN..." << std::endl;
                std::cout << expression << " >> " << postfix << std::endl; break;
        case 2: std::cout << "Evaluating expression..." << std::endl;
                std::cout << evaluate(postfix) << std::endl; break;
        case 3: std::cout << "Printing syntax tree..." << std::endl;
                print_tree(postfix); std::cout << std::endl; break;
        default: std::cout << "Wrong input please start over" << std::endl;
                get_input();
    }
}

std::string in_to_post(std::string infix) {
    std::string exp;
    std::queue<char> my_queue;
    int size = infix.length();
    for (int i = 0; i < size; ++i) {
        char temp = infix[i];
        if (temp != ' ') {
            my_queue.push(temp);
        } else {
            continue;
        }
    }
    std::stack<char> my_stack;

    while (!my_queue.empty()) {
        if (my_queue.size() == 0) {
            std::string top(1, my_stack.top());
            exp = exp + top;
            my_stack.pop();
        } else if (is_num(my_queue.front())) {
            std::string front(1, my_queue.front());
            exp += front;
            my_queue.pop();
        } else if (my_queue.front() == '(') {
            my_stack.push(my_queue.front());
            my_queue.pop();
        } else if (is_op(my_queue.front())){
            if (my_stack.empty()) {
                my_stack.push(my_queue.front());
                my_queue.pop();
            } else {
                for (int i = 0; i < my_stack.size()-1; i++){
                    if (get_precedence(my_queue.front()) <= get_precedence(my_stack.top())) {
                        if (my_stack.top() == '(') {
                            my_stack.pop();
                        }
                        std::string top(1, my_stack.top());
                        exp += top;
                        my_stack.pop();
                    } else {
                        break;
                    }
                }
                my_stack.push(my_queue.front());
                my_queue.pop();
            }
        } else if (my_queue.front() == ')') {
            while(!my_stack.empty() && my_stack.top() != '(') {
                if (my_stack.top() != '(') {
                    std::string top(1, my_stack.top());
                    exp += top;
                    my_stack.pop();
                }
            }
            my_queue.pop();
        } else {
            std::string top(1, my_stack.top());
            exp += top;
            my_stack.pop();
        }
    }
    std::string top(1, my_stack.top());
    exp += top;
    my_stack.pop();
    return exp;
}

bool is_num(char element) {
    if (element == '0' || element == '1' || element == '2' || element == '3' ||
            element == '4' || element == '5' || element == '6' ||
            element == '7' || element == '8' || element == '9' ) {
        return true;
    } else {
        return false;
    }
}

bool is_op(char element) {
    if (element == '+' || element == '-' || element == '*' || element == '/' ||
            element == '^') {
        return true;
    } else {
        return false;
    }
}

int get_precedence(char element) {
    if (element == '+' || element == '-') {
        return 0;
    } else if (element == '*' || element == '/') {
        return 1;
    } else if (element == '^') {
        return 2;
    } else if (element == '(' || element == ')') {
        return 3;
    } else {
        return -1;
    }
}

/**
 * Evaluate a postfix expression
 * @param  postfix The expression to evaluate
 * @return The value of the expression
 */
int evaluate(std::string postfix) {
    std::stack<int> my_stack;
    int value = 0;
    for (int i = 0; i < postfix.length(); i++) {
        char temp = postfix[i];
        if (temp != ' ') {
            if (is_num(temp)) { // This is a operator
                my_stack.push(temp - '0'); // Converting char to int.
            } else {
                int num_two = my_stack.top();
                my_stack.pop();
                int num_one = my_stack.top();
                my_stack.pop();
                value = evaluate(temp, num_two, num_one);
                my_stack.push(value);
            }
        }
    }
    return value;
}

/**
 * Evaluate the children of a operator.
 * @param  op the operation to perform
 * @param  x  the first operand
 * @param  y  the second operand
 * @return the value of the operation
 */
int evaluate(char op, int x, int y) {
    switch (op) {
       case '*': return x * y;
       case '/': return x / y;
       case '+': return x + y;
       case '-': return x - y;
       case '^': return (int)pow(x,y);
       default : return 0;
    }
}

/**
 * Performs the breadth-first algorithm to print out the tree level by level
 * @param postfix the expression to print.
 */
void print_tree(std::string postfix) {
    std::stack<exp_node*> track;
    // traverse string, check if each element is a number or operator,
    // add each type to its appropriate location in the tree.
    for (int i = 0; i < postfix.length(); i++) {
        char temp_char = postfix[i];
        if (is_num(temp_char)){
            // number should be a leaf node
            exp_node* temp = new exp_node(temp_char - '0');
            track.push(temp);
        } else {
            //operator should be a parent node
            exp_node* right = track.top();
            track.pop();
            exp_node* left = track.top();
            track.pop();
            exp_node* Op = new exp_node(postfix[i], left, right);
            track.push(Op);
        }
    }
    btree Tree;
    Tree.insert(track.top());
    Tree.printPretty(1, 0);
}


/**
 * This program works only with simple expressions
(expressions with only one pair of parenthesis and a limited amount of operations)
A tested example of this is (1+2)*3
---------------------------------------------------------------------


Welcome to the Expression trees!
--------------------------------------
Please enter an equation: (1+2)*3-4
What would you like to do with the expression? 
(1) Convert to RPN    (2) Evaluate    (3) Print the syntax tree: 3
Printing syntax tree...

        _______-______
       /              \
    ___*__             4  
   /      \
  _+       3
 /  \
 1   2

 */
