#include "./bloom.h"

/**
 * Adds a new element to the array. Using the optimal amount of hash functions,
 *         we hash the insertion element each time and set the bit in the
 *         index of the bit array equal to 1.
 * @param insert_element Element we wish to add
 */
void bloom::insert(std::string insert_element) {
    barray[hash_one(insert_element)] = 1;
    barray[hash_two(insert_element)] = 1;
    barray[hash_three(insert_element)] = 1;
    barray[hash_four(insert_element)] = 1;
    barray[hash_five(insert_element)] = 1;
    barray[hash_six(insert_element)] = 1;
}

/**
 * Determines if, and only if, an element exists within an array. By checking
 *         each index, we know that if an element exists all resulting indicies
 *         from the hash function will be 1, so any that are not would mean the
 *         element is not in the bit array
 * @param query_element Element we are searching for
 */
bool bloom::query(std::string query_element) {

    if (barray[hash_one(query_element)] == 0 ||
            barray[hash_two(query_element)] == 0 ||
            barray[hash_three(query_element)] == 0 ||
            barray[hash_four(query_element)] == 0 ||
            barray[hash_five(query_element)] == 0 ||
            barray[hash_six(query_element)] == 0) {
        return false;
    }
    return true;
}

int bloom::hash_one(std::string hash_element) const {
    // RS HASH
     unsigned int b    = 37855;
     unsigned int a    = 6368;
     unsigned int hash = 0;

     for(std::size_t i = 0; i < hash_element.length(); i++)
     {
        hash = hash * a + hash_element[i];
        a    = a * b;
     }

     return hash;
}

int bloom::hash_two(std::string hash_element) const {
    //DEK Hash
    unsigned int hash = hash_element.length();
    unsigned int i    = 0;

    for(i = 0; i < hash_element.length(); i++)
    {
       hash = ((hash << 5) ^ (hash >> 27)) ^ (hash_element[i]);
    }
    return hash;
}

int bloom::hash_three(std::string hash_element) const {
    //BPHash
     unsigned int hash = 0;
     unsigned int i    = 0;
     for(i = 0; i < hash_element.length(); i++)
     {
        hash = hash << 7 ^ (hash_element[i]);
     }

     return hash;
}

int bloom::hash_four(std::string hash_element) const {
    // FNV Hash
    const unsigned int fnv_prime = 0x87;
    unsigned int hash      = 0;
    unsigned int i         = 0;

    for(i = 0; i < hash_element.length(); i++)
    {
       hash *= fnv_prime;
       hash ^= (hash_element[i]);
    }

    return hash;
}

int bloom::hash_five(std::string hash_element) const {
    // AP Hash
    unsigned int hash = 0xAAA;
    unsigned int i    = 0;

    for(i = 0; i < hash_element.length(); i++)
    {
      hash ^= ((i & 1) == 0) ? (  (hash <<  7) ^ (hash_element[i]) * (hash >> 3)) :
                               (~((hash << 11) + ((hash_element[i]) ^ (hash >> 5))));
    }

    return hash;
}

int bloom::hash_six(std::string hash_element) const {
    //JS Hash
    unsigned int hash = 13154;
    unsigned int i    = 0;

    for(i = 0; i < hash_element.length(); i++)
    {
       hash ^= ((hash << 5) + (hash_element[i]) + (hash >> 2));
    }

    return hash;
}
