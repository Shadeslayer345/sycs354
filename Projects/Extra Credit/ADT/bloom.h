#include <bitset>
#include <string>

#ifndef SYCS354_PROJECTS_EXTRA_CREDIT_ADT_BLOOM_H
#define SYCS354_PROJECTS_EXTRA_CREDIT_ADT_BLOOM_H

/** Prettifying the hash function array */
//typedef int (*HASHFUNCTIONWITHONEPARAMETER) (std::string a);

/**
 * Bloom filter implementation.
 * TODO(Barry, Errol, Bolu): Implement more accurate hashfunctions!
 *
 * Algorithm:
 * The bloom filter simple allows a fast and space conservative way to determine
 *         if an element exist in a list. This saves programmers the high cost
 *         of doing a full search in the even that the element does not exist.
 *         A bit array of size (m) with (n) elements is implemented based on the
 *         amount of false-positives (p) we are ok with, and (k) hash functions
 *         are used based on (m). For ever element we wish to store, we hash the
 *          element (k) times and using the int result set that bit in our bit
 *          array equal to one.
 *
 * Math:
 *     Array size (m) = (n * ln(p)) / ln(2)^2 = 958505 bits or ~120Kb
 *     # of hash functions (k) = (m/n) * ln(2) = ~6 hash functions
 *
 */
class bloom {
 public:
    void insert(std::string insert_element);
    bool query(std::string query_element);
 private:
    int hash_one(std::string hash_element) const;
    int hash_two(std::string hash_element) const;
    int hash_three(std::string hash_element) const;
    int hash_four(std::string hash_element) const;
    int hash_five(std::string hash_element) const;
    int hash_six(std::string hash_element) const;
    std::bitset<958505> barray;

};
#endif
