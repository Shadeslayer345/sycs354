#include <iostream>

#include "ADT/bloom.h"

int main() {
    bloom bfil;
    bfil.insert("a");
    std::cout <<
            ((bfil.query("a")) ? "Element may exist":"Element does not exist")
            << std::endl;

    std::cout <<
            ((bfil.query("c")) ? "Element may exist":"Element does not exist")
            << std::endl;

    return 0;
}
