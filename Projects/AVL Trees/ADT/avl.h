/** Copyright 2015 Boluwatife Aiki-Raji Errol Grannum Barry C. Harris Jr. */
#include <cstddef>

#ifndef PROJECTS_AVL_TREES_ADT_AVL_H_
#define PROJECTS_AVL_TREES_ADT_AVL_H_
/**
 * Nodes to be inserted in the BST tree, containing a data member and two
 *         pointers to a left and right children.
 */
class node {
 public:
    node();
    ~node();

    int data;
    int height;
    node * left;
    node * right;
};

/**
 * The AVL, auto balancing tree, that builds on the Binary Search Tree by
 *         allowing the tree to always remain balanced.
 */
class avl {
 public:
    avl();
    ~avl();

    void insert(int insert_element_data);
    void remove(int element_to_remove);
    node * lookup(int search_element_data);
 private:
    void insert_(node *& compare_element, int insert_element_data);
    node * lookup_(node * search_root, int search_element_data);
    void adjust_(node *& adjustment_root);
    void rotate_right_(node *& rotate_root);
    void rotate_left_(node *& rotate_root);
    int height_(node * height_root) const;
    int balance_factor_(node * balance_root) const;
    void adjust_height_(node *& height_root);
    node * find_smallest_node_(node * remove_root);
    void remove_smallest_node_(node *& remove_root);
    void remove_(node *& remove_node, int element_to_remove);
    void destroy_(node * destroy_root);

    node * root_;
};
#endif  // PROJECTS_AVL_TREES_ADT_AVL_H_
