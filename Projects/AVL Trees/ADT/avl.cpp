/** Copyright 2015 Boluwatife Aiki-Raji Errol Grannum Barry C. Harris Jr. */
#include "./avl.h"
#include <iostream>

/**
 * Instantiates all members of the node class.
 * @constructor
 */
node::node() {
    this->data = 0;
    this->height = 1;
    this->left = NULL;
    this->right = NULL;
}

/**
 * Deallocates memory associated with the created node.
 */
node::~node() {
    delete left;
    delete right;
}

/**
 * Creates a new AVL tree, sets root node to NULL.
 */
avl::avl() {
    root_ = NULL;
}

/**
 * Deallocates memory associated with the AVL tree.
 */
avl::~avl() {
    delete root_;
}

/**
 * Wrapper function for the insert member function, keeps client code from
 *         needing access to the root node.
 * @param insert_element_data The data portion of a new BST node pointer
 */
void avl::insert(int insert_element_data = 0) {
    insert_(root_, insert_element_data);
}

/**
 * Wrapper function for the remove member function, keeps client code from
 *         needing to access the root node.
 * @param remove_element_data The value to be used when searching the BST nodes.
 */
void avl::remove(int remove_element_data = 0) {
    remove_(root_, remove_element_data);
}

/**
 * Wrapper function for the lookup member function, keeps client code from
 *         needing to access the root node.
 * @return The found node
 */
node* avl::lookup(int search_element_data = 0) {
    return lookup_(root_, search_element_data);
}

/**
 * Recursive function to add a new element into the Binary Search Tree, while
 *         also keeping the tree balanced.
 * @param insert_element_data The data portion of a new BST node pointer
 */
void avl::insert_(node *& compare_element, int insert_element_data) {
    if (compare_element == NULL) {
        node * new_element = new node();
        new_element->data = insert_element_data;
        compare_element = new_element;
        return;
    } else {
        if (insert_element_data < compare_element->data) {
            insert_(compare_element->left, insert_element_data);
        } else {
            insert_(compare_element->right, insert_element_data);
        }
    }
    adjust_(compare_element);
}

/**
 * Search the BST for a given node.
 * @return Either:
 *             The node being searched for
 *             NULL
 *
 */
node* avl::lookup_(node * search_root, int search_element_data) {
    if (search_root == NULL || search_root->data == search_element_data) {
        return search_root;
    } else if (search_element_data < search_root->data) {
        return lookup_(search_root->left, search_element_data);
    } else {
        return lookup_(search_root->right, search_element_data);
    }
}

/**
 * Private method to initiate the adjustment of the BST.
 */
void avl::adjust_(node *& adjustment_root) {
    adjust_height_(adjustment_root);
    int current_balance_factor = balance_factor_(adjustment_root);

    if (current_balance_factor == 2) {
        if (balance_factor_(adjustment_root->right) < 0) {
            rotate_right_(adjustment_root->right);
        } else {
            rotate_left_(adjustment_root);
        }
    } else if (current_balance_factor == -2) {
        if (balance_factor_(adjustment_root->left) > 0) {
            rotate_left_(adjustment_root->left);
        } else {
            rotate_right_(adjustment_root);
        }
    }
}

/**
 * Private method that performs a right rotation using the parametric node as
 *         the root.
 * @param rotate_root The node to be used as the center of the rotation
 */
void avl::rotate_right_(node *& rotate_root) {
    node * temp = rotate_root->left;
    rotate_root->left = temp->right;
    temp->right = rotate_root;
    adjust_height_(rotate_root);
    adjust_height_(temp);
    rotate_root = temp;
}

/**
 * Private method that performs a left rotation using the parametric node as
 *         the root.
 * @param rotate_root The node to be used as the center of the rotation
 */
void avl::rotate_left_(node *& rotate_root) {
    node * temp = rotate_root->right;
    rotate_root->right = temp->left;
    temp->left = rotate_root;
    adjust_height_(rotate_root);
    adjust_height_(temp);
    rotate_root = temp;
}

/**
 * A wrapper function for the height data member of the node class, allows
 *     operations with NULL pointers.
 * @param  height_root The node whose height we are trying to determine
 * @return height of the current node
 */
int avl::height_(node * height_root) const {
    return height_root ? height_root->height : 0;
}

/**
 * Finds the balance factor of the paremetric node
 * @param  balance_root The node whose balance factor we are determining
 * @return The balance factor of the current node
 */
int avl::balance_factor_(node * balance_root) const {
    return height_(balance_root->right) - height_(balance_root->left);
}

/**
 * Adjusts the height of a node after balancing the BST.
 * @param balance_root The root whose subtree is out of balance
 */
void avl::adjust_height_(node *& height_root) {
    int height_root_left_height = height_(height_root->left);
    int height_root_right_height = height_(height_root->right);

    height_root->height =
            ((height_root_left_height > height_root_right_height) ?
            height_root_left_height : height_root_right_height) + 1;
}

/**
 * When removing an node, we must find the smallest node in it's right
 *         subtree, this will take the place of the removed node.
 * @param  remove_root The to be removed node's right subtree.
 */
node *  avl::find_smallest_node_(node * remove_root) {
    return (remove_root->left != NULL) ?
            find_smallest_node_(remove_root->left) : remove_root;
}

/**
 * Once the smallest element (from find_smallest_node) is found, we must remove
 *         it from the tree and adjust accordingly.
 * @param remove_root The parent node whose subtree we are removing from
 */
void avl::remove_smallest_node_(node *& remove_root) {
    if (remove_root->left == NULL) {
        remove_root = remove_root->right;
        return;
    }
    remove_smallest_node_(remove_root->left);
    adjust_(remove_root);
}

/**
 * Search the BST, using the provided number as a filter and remove the FIRST,
 *         node that matches the number. Upon removal of the node we first find
 *         the smallest node it it's right subtree and place that into the
 *         removed node's position, the subtree is then balanced.
 * @param remove_element_data The value to be used when searching the BST nodes.
 */
void avl::remove_(node *& remove_root, int remove_element_data) {
    if (remove_root == NULL) {
        return;
    } else if (remove_element_data < remove_root->data) {
        remove_(remove_root->left, remove_element_data);
    } else if (remove_element_data > remove_root->data) {
        remove_(remove_root->right, remove_element_data);
    } else {
        node * remove_root_left = remove_root->left;
        node * remove_root_right = remove_root->right;
        if (remove_root_right == NULL) {
            remove_root = remove_root_left;
            return;
        } else {
            node * smallest_right_node =
                    find_smallest_node_(remove_root_right);
            remove_smallest_node_(remove_root_right);
            smallest_right_node->left = remove_root_left;
            remove_root = smallest_right_node;
            adjust_(remove_root);
        }
    }
    return;
}
