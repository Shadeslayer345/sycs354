/** Copyright 2015 Boluwatife Aiki-Raji Errol Grannum Barry C. Harris Jr. */
#include <iostream>

#include "ADT/avl.h"

int main() {
    avl avl_tree;

    avl_tree.insert(25);
    avl_tree.insert(1);
    avl_tree.insert(75);
    avl_tree.insert(50);
    avl_tree.insert(40);
    avl_tree.insert(60);
    avl_tree.insert(100);
    avl_tree.insert(90);
    avl_tree.insert(120);

    node * lookup = avl_tree.lookup(25);
    node * lookup2 = avl_tree.lookup(75);

    std::cout << "Found node1! Data = " << lookup->data << std::endl;
    std::cout << "Found node2! Data = " << lookup2->data << std::endl;

    avl_tree.remove(75);

    node * lookup3 = avl_tree.lookup(75);

    std::cout << "no NODE3! Data = "  << ((lookup3)? lookup->data : 0)
            << std::endl;

    return 0;
}
